﻿using System;

namespace Factory.Parts
{
    public class Wheels18inch : Wheels
    {
        public Wheels18inch()
        {
            Console.WriteLine("Wheels are R18");
        }
    }
}
