﻿using System;

namespace Factory.Parts
{
    public class Wheels16inch : Wheels
    {
        public Wheels16inch()
        {
            Console.WriteLine("Wheels are R16");
        }
    }
}
