﻿using System;

namespace Factory.Parts
{
    public class Wheels17inch : Wheels
    {
        public Wheels17inch()
        {
            Console.WriteLine("Wheels are R17");
        }
    }
}
